# Nebulus
## Set Us As A Peer!
In order to keep Nebulus from becoming just another "holder-of-files" make sure to set us as one of your IPFS peers:
`ipfs swarm connect /ip4/139.99.131.59/tcp/6537/ipfs/QmYUTAbwZWck3LW9XZBcHTz2Jaip3mGfYDt3LTXdPLEh23`

## Setting up a Nebulus Node:
Docker file is ready for testing!

Find it at `docker pull jrswab/nebulus`

If you want to be in the loop as a node contact J. R. Swab:

Discord `@jrswab#3134`, Tox: `jrswab@toxme.io`, or XMPP: `jrswab@kode.im`.

## Using Docker:
A Linux server is needed to run the Docker image since the image uses an Ubuntu base and thus needs an underlying Linux kernel. 
You can create an account at Digital Ocean with [this link](https://go.jrswab.com/ocean) to get a free $10 to use toward any server. 
There is also [Privex](https://www.privex.io/), they accept cryptocurrency including STEEM for payment.

### Dependencies:
* Linux
* Docker

### Before running the Docker image:
* Install Docker ([Here you can find the walk through for Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/)).
* Open ports `6537` and `3962` if you have a firewall.
* Create a working directory (`~/nebulus` works great).
* Inside working directory create another called `storage` (eg: `~/nebulus/storage`).
  * This directory is mounted into the Docker container
  * Allows us not to loose pins when the container shuts down.
* Download `/docker/run.sh` into your working directory to run Docker with all its glorious flags.
* Download Docker image with `docker pull jrswab/nebulus`.
* While inside your working directory run `./run.sh`
  * If this sends back an error execute `chmod +x run.sh`
  * You will end inside of the Ubuntu Docker image to execute a few more commands.

### Once Inside The Ubuntu Docker:
* Run `./ipfs_run.sh` to take care of the following:
  * Download IPFS
  * Extracting the IPFS tarball
  * Installing IPFS.
  * Initializing IPFS.
  * Updating the IPFS config.
  * Remove unneeded files.
* Optional: run `tmux`
  * This allows for the multiple terminal windows inside the docker container.
  * That way you can run the nebulus app in one window or pane and run something else like `htop` in another.
  * [Here you will find information on how to use tmux](https://robots.thoughtbot.com/a-tmux-crash-course)
* Run `nebulus_run.py`
  * Input Your Steem account name
  * Input your private posting key (no other key should be added)
  * Keep this open
    * You can let the docker run in the background by holding down `ctrl` and pressing `pq`.
    * Alternatively you can run tmux on your Linux server before running the docker and leave that tmux window open.