# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [2.3.0]()
### Added
- push script to automate docker push of both latest and version number
- getpass module so the private posting key is not visible in the terminal

### Changed
- The docker tag from a version to 'latest'

## [2.2.1](https://gitlab.com/jrswab/nebulus/commit/9d80666290e85133ab1bdfc0cffa68f02f8678c8)
### Added
- Removal of apt cach in docker image

### Fixed
- Function name typo in pin.py
- Lowered the number of layers in Dockerfile

## [2.2.0](https://gitlab.com/jrswab/nebulus/commit/1f9a325f56b64116f3c7d77cfab25ef7f557abbb)
### Added
- Dockerfile for node docker image creation.
- Modules directory
- run.sh to remove the need to copy/paste the long docker command.
- ipfs prep and install script
- nebulus prep and run script
- run.sh to make runnig the docker command easier.

### Changed
- Moved broadcast.py to modules directory
- broadcast.py to run as a module inside pin.py
- README.md to reflect proper ipfs config changes.
- Moved files for better organization
- Set all node scripts as modules to run in a startup script.

### Removed
- All website files. They are now found at [gitlab.com/jrswab/nebulus-website](https://gitlab.com/jrswab/nebulus-website)

### Fixed
- Various bugs in the python code and added checks to be sure of the correct state.

## [2.1.0](https://gitlab.com/jrswab/nebulus/commit/8a1bbc6c900c49153168eac00a3e885fa5215591) - 2018-9-11
### Added
- Setup script

## [2.0.1](https://gitlab.com/jrswab/nebulus/commit/ffbb3b7c5e4c8c9b44386f2fd6f575590cd91fa6) - 2018-9-10
### Changed
- Website landing page copy.

## [2.0.0](https://gitlab.com/jrswab/nebulus/commit/111c2c7b72466519ed62717bb652f8c8fec50f8e) - 2018-9-10
### Added
- Ability to pin hashes on home page without an account
- Node directory to hold node scripts and for future Docker file.
- Ability to send node live status with custom json for future needs.
- A config file to hold user info such as steem account and wif location.
- Script to repuest payment from Nebulus

### Changed
- pin.py to remove unneeded lines and modules
- execs/pin.py to node/pin.py for furture Docker file
- All files called by python for data now held in config/

### Removed
- Creation of new user profile pages
- Creation of new user feeds
- Login link for user accounts
- User file upload uploads

### Deprecated
- RSS feeds
- Profile pages
- New user account creation

## [1.0.0](https://gitlab.com/jrswab/nebulus/commit/b585d3b4137c354e17e930dbb5bb766ebf3dbffc) - 2018-08-27
### Added
- This changelog
