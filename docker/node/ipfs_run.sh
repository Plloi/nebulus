#!/bin/bash

#download IPFS
wget 'https://dist.ipfs.io/go-ipfs/v0.4.17/go-ipfs_v0.4.17_linux-amd64.tar.gz'

# extract ipfs
echo 'Extracting IPFS'
echo ''
tar -xvzf go-ipfs*.tar.gz

# install IPFS
echo 'Installing IPFS'
echo ''
mv ./go-ipfs/ipfs /usr/local/bin
rm -rf go-ipfs*

# init ipfs and save output to file
echo 'Setting up IPFS'
echo ''
ipfs init

# run ipfs deamon
echo 'Starting the IPFS Daemon'
echo ''
ipfs daemon &

sleep 6

# ipfs config settings
## Tell IPFS not to use local network discovery
ipfs config --json Discovery.MDNS.Enabled false 2>&1

## Set IPFS to filter out common local IP addresses
ipfs config --json Swarm.AddrFilters '[
	"/ip4/10.0.0.0/ipcidr/8",
	"/ip4/100.64.0.0/ipcidr/10",
	"/ip4/169.254.0.0/ipcidr/16",
	"/ip4/172.16.0.0/ipcidr/12",
	"/ip4/192.0.0.0/ipcidr/24",
	"/ip4/192.0.0.0/ipcidr/29",
	"/ip4/192.0.0.8/ipcidr/32",
	"/ip4/192.0.0.170/ipcidr/32",
	"/ip4/192.0.0.171/ipcidr/32",
	"/ip4/192.0.2.0/ipcidr/24",
	"/ip4/192.168.0.0/ipcidr/16",
	"/ip4/198.18.0.0/ipcidr/15",
	"/ip4/198.51.100.0/ipcidr/24",
	"/ip4/203.0.113.0/ipcidr/24",
	"/ip4/240.0.0.0/ipcidr/4"
]' 2>&1

exit 0
