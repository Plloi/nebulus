#!/usr/bin/env python3

import getpass
import subprocess

def main():
    print('Enter Your Steem Account Name')
    steemName = input()

    print('Enter your private posting key.' + '\n' + 
        ' WARNING: DO NOT ADD ANY OTHER PRIVATE KEY!')
    wif = getpass.getpass()

    with open('config/userData', 'w') as userData:
        userData.write(steemName + '\n' + wif + '\n')

if __name__ == '__main__':
    main()
