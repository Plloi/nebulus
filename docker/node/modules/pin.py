#!/usr/bin/env python3

import subprocess
import time
import random
import datetime

# custom Nebulus modules
from modules import broadcast

# To escape shell strings
from shlex import quote

# BEEM modules
from beem.account import Account

# sent Nebulus account
acc = Account("nebulus")

# TODO: Change to be $1 usd worth of steem
# instead of one steem.
def priceCheck(amount):    
    chainData = amount.split()
    total = float(chainData[0])
    # make sure correct currency was sent
    if 'STEEM' in chainData[1]:
        if total >= 1:
            return True
        else:
            return False
    else:
        return False

def getTime(stamp):
    # Check to make sure the pin is not too old
    # and past payout
    date = stamp[0].split('-')
    reqDay = datetime.date(int(date[0]), int(date[1]), int(date[2]))
    dt = datetime.timedelta(-1)
    now = datetime.datetime.now()
    today = datetime.date(now.year, now.month, now.day)

    if reqDay < (today + dt):
        return True
    else:
        return False

def randNum():
    return random.randint(0, 100)

# get IPFS hash from the transfer memo
def getHash(memo):
    ipfsHash = memo.split()
    return ipfsHash[1]

# look for hash in server
def hashCheck(ipfsHex):
    return subprocess.call('/usr/local/bin/ipfs pin ls | grep '
        + format(quote(ipfsHex)), shell=True)

def checkSubProc(pin, count):
    # if still running Popen.poll() returns None
    # else it ouputs the returncode of the command ran.
    if pin.poll() == None:
        time.sleep(1)
        # for looping on itself
        count += 1
        if count <= 600:
            checkSubProc(pin, count)
        else:
            return pin.poll()
    else:
        return pin.poll()

def findPID():
    pgrep = subprocess.check_output('pgrep -an ipfs', shell=True)
    return str(pgrep.split()[0]).split('\'')

def kill(pid):
    kill = pid[1]
    subprocess.call('kill -9 ' + format(quote(kill)), shell=True)

def main():
    for blocks in acc.get_account_history(-1, 500):
        # skip any block without these parameters
        # using dict.get() to avoid errors when a block
        #   does not have a specified key.
        if 'transfer' in blocks.get('type', 'none'):
            if 'pin ' in blocks.get('memo', 'none'):
                stamp = blocks.get('timestamp', 'none').split('T')
                if getTime(stamp):
                    print('Requested hash is outside of payment window. Skipping')
                    # to keep all nodes from filling up at the same rate
                elif priceCheck(blocks['amount']) and randNum() % 2 == 0:
                    # set current memo hash to variable
                    currHash = getHash(blocks['memo'])
                    # hashCheck == 1 tells us that the hash was not found
                    if hashCheck(currHash) == 1:
                        pin = subprocess.Popen('/usr/local/bin/ipfs pin add ' +
                            format(quote(currHash)), shell=True)
                        # give ipfs time to pin hash (in seconds)
                        status = checkSubProc(pin, 0)
                        # ipfs returns '0' when successful
                        if status == 0:
                            # if pinned write hash to file
                            with open('config/hashList', 'a') as hashList:
                                hashList.write(currHash)
                        elif status == 1:
                            print('Pin exited with IPFS error code 1')
                        else:
                            # kill pin process if still running
                            kill(findPID())

    # Send pinned hashes to the blockchain
    with open('config/hashList', 'r') as listCheck:
        checkList = listCheck.read().replace('\n', '')
    
    if checkList == '':
        print('Nothing to send to the blockchain this round.')
    else:
        broadcast.send()

    # have script run once every hour
    print('Script pausing for one hour...')
    time.sleep(3600)
    # rerun the script
    print('Executing script...')
    main()
    
if __name__ == '__main__':
    main()
