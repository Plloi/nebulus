#!/bin/bash
# # run docker
loc="$(readlink -f storage)"
docker container run -it \
	-v ${loc}:/root/.ipfs \
	-p 6537:4001 -p 3962:5001 \
	--name nebulus \
	--rm jrswab/nebulus:latest
